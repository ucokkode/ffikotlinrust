#include <jni.h>
#include <string>
#include <iostream>
#include <android/log.h>
#include <thread>
#include <optional>
#include <vector>


//#ifdef NDEBUG
#define LOG_TAG ("MYCLASS")
#define LOG_D(args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, args)
#define LOG_E(args...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, args)

//#endif
struct MyClass {
private:
    int thread_id = 0;
    int i;
public:
    explicit MyClass(int id) : i(0), thread_id(id) {
        LOG_D("my int is %d on thread %d", i, thread_id);
    }

    inline void inc() {
        i++;
    }

    ~MyClass() {
        LOG_D("goodbye on %d in thread %d", i, thread_id);
    }
};


thread_local std::optional<MyClass> my_class = std::nullopt;
static uint32_t num_cpu = std::thread::hardware_concurrency();

void do_thing(int id) {
    my_class = *new MyClass(id);
    if (my_class.has_value()) {
        for (int i = 0; i < UINT16_MAX; ++i) {
            my_class.value().inc();
        }
    }
}

inline void printViaKotlin(JNIEnv *env, jobject that, int thread_id) {
    jclass cls = env->GetObjectClass(that);
    if (cls != nullptr) {
        std::string method_name = "showId";
        jmethodID show_id = env->GetMethodID(cls, method_name.c_str(), "(I)V");
        if (show_id != nullptr) {
            LOG_D("method %s found", method_name.c_str());
            env->CallVoidMethod(that, show_id, thread_id);
        } else {
            LOG_E("method %s not found", method_name.c_str());
        }
    } else {
        LOG_E("failed to get class");
    }

}

inline void runThread(JNIEnv *env, jobject that) {
    LOG_D("cpu cores are %d", num_cpu);
    std::vector<std::thread> ths;
    ths.reserve(num_cpu);
    for (int i = 0; i < num_cpu; ++i) {
        ths.emplace_back(do_thing, i);
        printViaKotlin(env, that, i);
    }
    for (auto &th: ths) {
        th.join();
    }
}


extern "C" {
JNIEXPORT jstring JNICALL
Java_com_example_nativelib_NativeLib_stringFromJNI(JNIEnv *env, jobject that) {
    std::string message = "from native, your cpu's are ";
    message += std::to_string(num_cpu);
    std::shared_ptr<std::string> hello = std::make_shared<std::string>(message);
    return env->NewStringUTF(hello->c_str());
}
JNIEXPORT void JNICALL
Java_com_example_nativelib_NativeLib_runCounting(JNIEnv *env, jobject that) {
    runThread(env, that);
}
}