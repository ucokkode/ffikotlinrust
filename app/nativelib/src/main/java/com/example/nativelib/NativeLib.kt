package com.example.nativelib

import android.content.Context
import android.util.Log
import android.widget.Toast

class NativeLib(private val context: Context) {
    // Used to load the 'nativelib' library on application startup.
    init {
        System.loadLibrary("nativelib")
    }
    /**
     * A native method that is implemented by the 'nativelib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String
    external fun runCounting()
    fun showId(thread_id: Int) {
        Toast.makeText(context, "thread $thread_id done", Toast.LENGTH_SHORT).show()
    }
}